import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { createStore, combineReducers } from "redux";
import { Provider } from "react-redux";
//B.1 khai báo reducer mới tạo:
import categoryReducer from "./store/reducers/categoryReducer";
import productReducer from "./store/reducers/productReducer";

//A.3. luôn tạo rootReducer, truyền tham số vào A.1
const rootReducer = combineReducers({
  //B.2 khai báo reducers con (key: value)
  categoryReducer,
  productReducer,
});

//A.1.tạo store
const store = createStore(
  rootReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

//A.2.phân phối store và app
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
