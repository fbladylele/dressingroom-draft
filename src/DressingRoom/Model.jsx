import React, { Component } from "react";
import "./model.css";
class Model extends Component {
  render() {
    return (
      <div
        style={{
          background: "url(./assets/img/background/background_998.jpg)",
        }}
        className="contain"
      >
        <div
          style={{ background: "url(./assets/img/allbody/bodynew.png)" }}
          className="body"
        />
        <div
          style={{ background: "url(./assets/img/model/1000new.png)" }}
          className="model"
        />
        <div
          style={{ background: "url(./assets/img/allbody/bikini_branew.png)" }}
          className="bikinitop"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/bikini_pantsnew.png)",
          }}
          className="bikinibottom"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/feet_high_leftnew.png)",
          }}
          className="feetleft"
        />
        <div
          style={{
            background: "url(./assets/img/allbody/feet_high_rightnew.png)",
          }}
          className="feetright"
        />
      </div>
    );
  }
}

export default Model;
