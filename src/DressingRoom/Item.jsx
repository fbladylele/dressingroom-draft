import React, { Component } from "react";

class Item extends Component {
  render() {
    // const{name, imgSrc_jpg}=this.props.prod
    return (
      <div className="card mb-3">
        <img src={this.props.prod.imgSrc_jpg} />
        <div className="card-body">
          <p className="lead">{this.props.prod.name}</p>
          <button className="btn btn-success">Thử</button>
        </div>
      </div>
    );
  }
}

export default Item;
