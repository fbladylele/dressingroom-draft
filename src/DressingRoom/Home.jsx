import React, { Component } from "react";
import List from "./List";
import Categories from "./Categories";
import Model from "./Model";

class Home extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-7">
            <Categories />
            <List />
          </div>
          <div className="col-5">
            <Model />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
