import React, { Component } from "react";
import Item from "./Item";
import { connect } from "react-redux";

class List extends Component {

  filterProducts = () => {
    // console.log(this.props.selectedType);
    // console.log(this.props.productList
    //   .filter((item) => {
    //     return item.type === this.props.selectedType;
    //   }));

    return this.props.productList
      .filter((item) => {
        return item.type === this.props.selectedType;
      })
      .map((item) => {
        return (
          <div key={item.type} className="col-4">
            <Item prod={item} />
          </div>
        );
      });
  };

  //-------------------------
  render() {
    return (
      <div className="container-fluid">
        <div className="row">{this.filterProducts()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    productList: state.productReducer.productList,
    selectedType: state.categoryReducer.selectedCategory,
  };
};
export default connect(mapStateToProps)(List);


