import React, { Component } from "react";
import { connect } from "react-redux";

class Categories extends Component {
  handleSelect = (chosenType) => {
    this.props.dispatch({
      type: "CHANGE_SELECTED_TYPE",
      payload: chosenType,
    });
  };

  renderCategories = () => {
    return this.props.categoryList.map((item) => {
      return (
        <button onClick={()=>this.handleSelect(item.type)} key={item.type} className="btn btn-secondary">
          {item.showName}
        </button>
      );
    });
  };

  render() {
    return (
      <div className="btn-group container-fluid mb-4">
        {this.renderCategories()}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    categoryList: state.categoryReducer.categoryList,
  };
};
export default connect(mapStateToProps)(Categories);
